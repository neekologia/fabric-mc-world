function fact(n)
    if n < 2 then
        return n 
    else
        return n * fact(n - 1)
    end
end

print(fact(5))
